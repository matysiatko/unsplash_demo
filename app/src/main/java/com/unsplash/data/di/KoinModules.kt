package com.unsplash.data.di

import com.unsplash.ui.main.mainModule

val koinModules = listOf(
        networkModule,
        utilsModule,
        mainModule
)
