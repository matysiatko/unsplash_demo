package com.unsplash.data.di

import android.annotation.SuppressLint
import com.unsplash.BuildConfig
import com.unsplash.data.network.Api
import com.unsplash.data.network.NetworkRepository
import com.unsplash.data.network.NetworkRepositoryImpl
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

val networkModule = module {
    single<Api> {
        val cacheSize = 10 * 1024 * 1024L
        val cache = Cache(androidContext().filesDir, cacheSize)

        val okHttpClientBuilder = OkHttpClient
                .Builder()
                .connectTimeout(TimeUnit.SECONDS.toMillis(5), TimeUnit.MILLISECONDS)
                .readTimeout(TimeUnit.SECONDS.toMillis(5), TimeUnit.MILLISECONDS)
                .writeTimeout(TimeUnit.SECONDS.toMillis(5), TimeUnit.MILLISECONDS)
                .cache(cache)

        // For debug build, enable logging network traffic and trust all certificates
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            okHttpClientBuilder.addInterceptor(logging)

            try {
                val trustManager = @SuppressLint("CustomX509TrustManager")
                object : X509TrustManager {
                    override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()

                    @SuppressLint("TrustAllX509TrustManager")
                    override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                    }

                    @SuppressLint("TrustAllX509TrustManager")
                    override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                    }
                }

                // Install the all-trusting trust manager
                val sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, arrayOf(trustManager), SecureRandom())
                // Create an ssl socket factory with all-trusting manager
                val sslSocketFactory = sslContext.socketFactory

                okHttpClientBuilder
                        .sslSocketFactory(sslSocketFactory, trustManager)
                        .hostnameVerifier { _, _ -> true }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        Retrofit
                .Builder()
                .client(okHttpClientBuilder.build())
                .baseUrl("https://api.unsplash.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Api::class.java)
    }

    single { NetworkRepositoryImpl(api = get()) } bind NetworkRepository::class
}
