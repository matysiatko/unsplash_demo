package com.unsplash.data.di

import com.unsplash.util.dipatcher.DispatcherProvider
import com.unsplash.util.dipatcher.DispatcherProviderImpl
import com.unsplash.util.error.ErrorReporter
import com.unsplash.util.error.ErrorReporterImpl
import org.koin.dsl.bind
import org.koin.dsl.module

val utilsModule = module {
    single { DispatcherProviderImpl() } bind DispatcherProvider::class

    single { ErrorReporterImpl() } bind ErrorReporter::class
}
