package com.unsplash.data.network

import com.unsplash.data.network.models.Photo
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("photos")
    suspend fun photosPerClient(@Query("client_id") clientId: String): List<Photo>
}
