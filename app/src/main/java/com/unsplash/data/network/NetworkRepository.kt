package com.unsplash.data.network

import com.unsplash.data.network.models.Photo

interface NetworkRepository {

    suspend fun photosPerClient(clientId: String): List<Photo>
}
