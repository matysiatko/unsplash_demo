package com.unsplash.data.network

import com.unsplash.data.network.models.Photo

class NetworkRepositoryImpl(private val api: Api) : NetworkRepository {

    override suspend fun photosPerClient(clientId: String): List<Photo> {
        return api.photosPerClient(clientId)
    }
}
