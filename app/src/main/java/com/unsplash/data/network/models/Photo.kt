package com.unsplash.data.network.models

data class Photo(
        val id: String,
        val urls: PhotoUrls
)
