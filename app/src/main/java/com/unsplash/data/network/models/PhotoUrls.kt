package com.unsplash.data.network.models

data class PhotoUrls(val thumb: String)
