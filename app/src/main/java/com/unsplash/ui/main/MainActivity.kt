package com.unsplash.ui.main

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.unsplash.R
import com.unsplash.ui.main.compose.MainScreen
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AppScreen {
                MainScreen(mainViewModel = viewModel)
            }
        }
    }

    @Composable
    fun AppScreen(content: @Composable () -> Unit) {
        Scaffold(
                topBar = { TopAppBar(title = { Text(text = stringResource(id = R.string.app_name)) }) },
                backgroundColor = Color.Black
        ) {
            content()
        }
    }
}
