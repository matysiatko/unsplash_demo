package com.unsplash.ui.main

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    viewModel {
        MainViewModel(
                mainUseCase = get(),
                errorReporter = get()
        )
    }
    factory {
        MainUseCase(
                networkRepository = get(),
                dispatcherProvider = get()
        )
    }
}
