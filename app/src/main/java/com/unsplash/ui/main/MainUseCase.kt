package com.unsplash.ui.main

import com.unsplash.data.network.NetworkRepository
import com.unsplash.ui.model.GalleryRowUI
import com.unsplash.ui.model.GalleryUI
import com.unsplash.ui.model.PhotoUI
import com.unsplash.util.dipatcher.DispatcherProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class MainUseCase(
        private val networkRepository: NetworkRepository,
        private val dispatcherProvider: DispatcherProvider
) {

    fun getGalleryUI(): Flow<GalleryUI> = flow {
        val galleryRows = networkRepository
                .photosPerClient(CLIENT_ID)
                .map { PhotoUI(it.urls.thumb) }
                .chunked(PHOTOS_PER_ROW)
                .map { GalleryRowUI(it) }

        val rowWeight = 1 / PHOTOS_PER_ROW.toFloat()

        emit(GalleryUI(galleryRows, rowWeight))
    }
            .flowOn(dispatcherProvider.io)

    companion object {
        private const val CLIENT_ID = "gkoGjkJctv1f1u5MnN2R73zk7IoyNL3R9IUVDX3-_Do"
        private const val PHOTOS_PER_ROW = 4
    }
}
