package com.unsplash.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.unsplash.ui.model.GalleryUI
import com.unsplash.ui.model.UIState
import com.unsplash.util.error.ErrorReporter
import kotlinx.coroutines.flow.*

class MainViewModel(
        private val mainUseCase: MainUseCase,
        private val errorReporter: ErrorReporter
) : ViewModel() {

    private val _galleryState = MutableStateFlow<UIState<GalleryUI>>(UIState.Loading())

    val galleryState = _galleryState.asStateFlow()
    val onError = { getGallery() }

    init {
        getGallery()
    }

    private fun getGallery() {
        mainUseCase
                .getGalleryUI()
                .onStart { _galleryState.value = UIState.Loading() }
                .catch {
                    errorReporter.report(it)
                    _galleryState.value = UIState.Error(it)
                }
                .onEach { _galleryState.value = UIState.Ready(it) }
                .launchIn(viewModelScope)
    }
}
