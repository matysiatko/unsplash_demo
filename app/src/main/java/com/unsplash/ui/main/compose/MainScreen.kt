package com.unsplash.ui.main.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import com.unsplash.ui.main.MainViewModel
import com.unsplash.ui.model.UIState.*

@Composable
fun MainScreen(mainViewModel: MainViewModel) {
    when (val state = mainViewModel.galleryState.collectAsState().value) {
        is Error -> ErrorButton(onErrorButtonClick = mainViewModel.onError)
        is Loading -> ProgressBar()
        is Ready -> PhotoGallery(galleryUI = state.data)
    }
}
