package com.unsplash.ui.main.compose

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.unsplash.R
import com.unsplash.R.string
import com.unsplash.ui.model.GalleryUI

@Composable
fun PhotoGallery(galleryUI: GalleryUI) {
    LazyColumn(Modifier.fillMaxSize()) {
        items(galleryUI.rows) { row ->
            Row(modifier = Modifier.fillMaxWidth()) {
                row.photos.forEach { photo ->
                    Image(
                            painter = rememberImagePainter(
                                    data = photo.url,
                                    builder = { placeholder(R.drawable.unsplash_logo) }
                            ),
                            contentDescription = null,
                            contentScale = ContentScale.Crop,
                            modifier = Modifier
                                    .fillParentMaxWidth(galleryUI.rowWeight)
                                    .aspectRatio(1f)
                                    .border(1.dp, Color.Black)
                                    .background(Color.Cyan)
                    )
                }
            }
        }
    }
}

@Composable
fun ProgressBar() {
    Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun ErrorButton(onErrorButtonClick: () -> Unit) {
    Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
    ) {
        OutlinedButton(onClick = onErrorButtonClick) {
            Text(text = stringResource(id = string.button_error))
        }
    }
}
