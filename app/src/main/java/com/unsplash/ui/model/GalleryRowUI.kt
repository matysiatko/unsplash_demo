package com.unsplash.ui.model

data class GalleryRowUI(val photos: List<PhotoUI>)
