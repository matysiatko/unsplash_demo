package com.unsplash.ui.model

data class GalleryUI(
        val rows: List<GalleryRowUI>,
        val rowWeight: Float
)
