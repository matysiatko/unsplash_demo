package com.unsplash.ui.model

data class PhotoUI(val url: String)
