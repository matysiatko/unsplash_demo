package com.unsplash.ui.model

sealed class UIState<T> {
    class Ready<T>(val data: T) : UIState<T>()
    class Loading<T> : UIState<T>()
    class Error<T>(val throwable: Throwable) : UIState<T>()
}
