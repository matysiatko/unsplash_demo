package com.unsplash.util.dipatcher

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class DispatcherProviderImpl : DispatcherProvider {

    override val io: CoroutineDispatcher get() = Dispatchers.IO
}
