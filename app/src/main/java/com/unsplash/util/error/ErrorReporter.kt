package com.unsplash.util.error

interface ErrorReporter {

    fun report(throwable: Throwable)
}
