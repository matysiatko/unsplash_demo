package com.unsplash.util.error

class ErrorReporterImpl : ErrorReporter {

    override fun report(throwable: Throwable) {
        throwable.printStackTrace()
        // possibly add Crashlytics
    }
}
