package com.unsplash.ui.main

import com.google.common.truth.Truth.assertThat
import com.unsplash.CoroutineTestRule
import com.unsplash.data.network.NetworkRepository
import com.unsplash.data.network.models.Photo
import com.unsplash.data.network.models.PhotoUrls
import com.unsplash.ui.model.GalleryRowUI
import com.unsplash.ui.model.GalleryUI
import com.unsplash.ui.model.PhotoUI
import com.unsplash.util.dipatcher.DispatcherProvider
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class MainUseCaseTest {

    @MockK
    private lateinit var networkRepository: NetworkRepository

    @MockK
    private lateinit var dispatcherProvider: DispatcherProvider

    private lateinit var mainUseCase: MainUseCase

    private val photos = listOf(
            Photo("1", PhotoUrls("https://images.unsplash.com/photo-1")),
            Photo("2", PhotoUrls("https://images.unsplash.com/photo-2")),
            Photo("3", PhotoUrls("https://images.unsplash.com/photo-3")),
            Photo("4", PhotoUrls("https://images.unsplash.com/photo-4")),
            Photo("5", PhotoUrls("https://images.unsplash.com/photo-5")),
            Photo("6", PhotoUrls("https://images.unsplash.com/photo-6")),
            Photo("7", PhotoUrls("https://images.unsplash.com/photo-7")),
            Photo("8", PhotoUrls("https://images.unsplash.com/photo-8")),
            Photo("9", PhotoUrls("https://images.unsplash.com/photo-9")),
            Photo("10", PhotoUrls("https://images.unsplash.com/photo-10"))
    )

    private val galleryUI = GalleryUI(
            listOf(
                    GalleryRowUI(
                            listOf(
                                    PhotoUI("https://images.unsplash.com/photo-1"),
                                    PhotoUI("https://images.unsplash.com/photo-2"),
                                    PhotoUI("https://images.unsplash.com/photo-3"),
                                    PhotoUI("https://images.unsplash.com/photo-4")
                            )
                    ),
                    GalleryRowUI(
                            listOf(
                                    PhotoUI("https://images.unsplash.com/photo-5"),
                                    PhotoUI("https://images.unsplash.com/photo-6"),
                                    PhotoUI("https://images.unsplash.com/photo-7"),
                                    PhotoUI("https://images.unsplash.com/photo-8")
                            )
                    ),
                    GalleryRowUI(
                            listOf(
                                    PhotoUI("https://images.unsplash.com/photo-9"),
                                    PhotoUI("https://images.unsplash.com/photo-10")
                            )
                    )
            ),
            0.25f
    )

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        mainUseCase = MainUseCase(networkRepository, dispatcherProvider)
    }

    @Test
    fun `GalleryUI should consists of rows of photos, each row 4 photos at max`() {
        every { dispatcherProvider.io } returns coroutineTestRule.testDispatcher
        coEvery { networkRepository.photosPerClient(any()) } returns photos

        coroutineTestRule.testDispatcher.runBlockingTest {
            mainUseCase
                    .getGalleryUI()
                    .collect { assertThat(it).isEqualTo(galleryUI) }
        }
    }
}
