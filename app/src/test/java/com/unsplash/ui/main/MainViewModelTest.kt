package com.unsplash.ui.main

import com.google.common.truth.Truth.assertThat
import com.unsplash.CoroutineTestRule
import com.unsplash.ui.model.UIState
import com.unsplash.util.error.ErrorReporter
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class MainViewModelTest {

    @MockK
    private lateinit var mainUseCase: MainUseCase

    @MockK
    private lateinit var errorReporter: ErrorReporter

    private lateinit var mainViewModel: MainViewModel

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `if getGalleryUI returns, galleryState is Ready`() {
        every { mainUseCase.getGalleryUI() } returns flowOf(mockk())

        coroutineTestRule.testDispatcher.runBlockingTest {
            initViewModel()

            assertThat(mainViewModel.galleryState.value).isInstanceOf(UIState.Ready::class.java)
        }
    }

    @Test
    fun `if getGalleryUI crashes, galleryState is Error and the crash reported`() {
        val error = Throwable("some crash when fetching photos")

        every { mainUseCase.getGalleryUI() } returns flow { throw error }
        every { errorReporter.report(any()) } just Runs

        coroutineTestRule.testDispatcher.runBlockingTest {
            initViewModel()

            assertThat(mainViewModel.galleryState.value).isInstanceOf(UIState.Error::class.java)
            verify { errorReporter.report(error) }
        }
    }

    private fun initViewModel() {
        mainViewModel = MainViewModel(mainUseCase, errorReporter)
    }
}
